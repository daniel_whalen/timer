#!/usr/bin/env python
# A simple timer that plays a alarm after a specified time
# Written by: Daniel Whalen
# Contact information: i3eaker@yahoo.com

# Libraries to import
from time import sleep
import os
import sys
from subprocess import call

# Launch the application from any directory
program_path = os.path.realpath(__file__)
program_dir = os.path.dirname(program_path)

# Begin the simple timer
print("Welcome to the simple timer!")

# Delay the program for the number of minutes specified
def run_timer():
    sleep(time_seconds)
    return

# Play the alarm sound
def alarm():
    if sys.platform == 'linux':
        call(["xdg-open",program_dir + "/alarm.wav"])
    elif sys.platform == 'darwin':
        call(["afplay",program_dir + "/alarm.wav"])
    else:
        os.system("start " + program_dir + "\alarm.wav")
    return

# Main loop, keeping the application running until stopped by the user
while 1 == 1:
    time_prompt = "Time till alarm (in minutes)- "
#    test = sys.platform
#    print (test)
    time = input(time_prompt)
    time_seconds = int(time) * 60
    run_timer()
    alarm()
